#include <elapsedMillis.h>

#define ANALOG_X A1
#define ANALOG_Y A0
#define ANALOG_BUTTON 3
#define TIMER_TIMESTAMP 200

volatile bool button = false;
//Timers
elapsedMillis analogReadTimer;
elapsedMillis interruptTimer;

void setup() {
  Serial.begin(9600);
  pinMode(ANALOG_BUTTON, INPUT_PULLUP);
  delay(1000);

  //set interrupts
  attachInterrupt(digitalPinToInterrupt(ANALOG_BUTTON), analog_button, FALLING);

  //reset timers
  analogReadTimer= 0;
  interruptTimer = 0;
  Serial.println("Start");
}

void loop() 
{
  analog_VR(10);
  delay(1);
}

void analog_VR(int timestamp)
{
  if(analogReadTimer> timestamp)
  {
    analogReadTimer = 0;
    Serial.print("X: "); Serial.println(analogRead(ANALOG_X));
    Serial.print("Y: "); Serial.println(analogRead(ANALOG_Y));
  }
}

void analog_button()
{
  if (digitalRead(ANALOG_BUTTON) == LOW && interruptTimer > TIMER_TIMESTAMP)
  {
    interruptTimer= 0;
    button = !button;
    Serial.println(button);
  }
  else
  {
  }
}
