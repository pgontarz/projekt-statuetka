#include <elapsedMillis.h>
#include <Adafruit_NeoPixel.h>
#include "settings.h"
#include "music.h"

Adafruit_NeoPixel strip(LED_COUNT, LED_PIN, NEO_GRBW + NEO_KHZ800);


void setup() {
  Serial.begin(9600);
  pinMode(ANALOG_BUTTON, INPUT_PULLUP);
  pinMode(Pin1, OUTPUT);  
  pinMode(Pin2, OUTPUT);  
  pinMode(Pin3, OUTPUT);  
  pinMode(Pin4, OUTPUT); 

  pinMode(Pin1_HIGH, OUTPUT);  
  pinMode(Pin2_HIGH, OUTPUT);  
  pinMode(Pin3_HIGH, OUTPUT);  
  pinMode(Pin4_HIGH, OUTPUT); 

  strip.begin();           
  strip.show();         
  strip.setBrightness(255);
  strip.fill(strip.Color(0, 0, 0));
  strip.show();
  attachInterrupt(digitalPinToInterrupt(ANALOG_BUTTON), analog_button, FALLING);
  delay(100);
  //-----------reset timers
  analogReadTimer= 0;
  interruptTimer =0;
  stepperTimer = 0;
  stepperUpTimer = 0;
  musicTimer = 0;
  musicDelayTimer = 0;

  X_ANALOG_BASE = analogRead(ANALOG_X);
  Y_ANALOG_BASE = analogRead(ANALOG_Y);

  //callibrateSteppers(&need_calibration);
  Serial.println("Start");

  colorWipe(strip.Color(255,   0,   0)     , 10); // Red
}

void loop() {
  
  callibrateSteppers(&need_calibration);
  moveAward(&automatic_mode);
  stepper_down_controller(stepper_timestamp);
  stepper_up_controller(stepper_up_timestamp);
  analog_VR(10);
}

void colorWipe(uint32_t color, int wait) {
  for(int i=0; i<strip.numPixels(); i++) { // For each pixel in strip...
    strip.setPixelColor(i, color);         //  Set pixel's color (in RAM)
    strip.show();                          //  Update strip to match
    delay(wait);                           //  Pause for a moment
  }
}

void analog_VR(int timestamp)
{
  if(analogReadTimer> timestamp)
  {
    analogReadTimer = 0;
    if(analogRead(ANALOG_X) >X_ANALOG_BASE+ 30 && analogRead(ANALOG_X)<= 1023)
    {
      stepper_running = true;
      dir = false;
      stepper_timestamp = map(analogRead(ANALOG_X), X_ANALOG_BASE, 1023, 8, 0);
    }    
    else if(analogRead(ANALOG_X) >=0 && analogRead(ANALOG_X)< X_ANALOG_BASE- 30)
    {
      stepper_running = true;
      dir = true;
      stepper_timestamp = map(analogRead(ANALOG_X), 0, 516, 0, 8);
    }    
    else
    {
      stepper_running = false;
    }

    if(analogRead(ANALOG_Y) >Y_ANALOG_BASE+ 30 && analogRead(ANALOG_Y)<= 1023)
    {
      stepper_running_up = true;
      dir_up = false;
      stepper_up_timestamp = map(analogRead(ANALOG_Y), Y_ANALOG_BASE, 1023, 8, 0);
    }    
    else if(analogRead(ANALOG_Y) >=0 && analogRead(ANALOG_Y)< Y_ANALOG_BASE- 30)
    {
      stepper_running_up = true;
      dir_up = true;
      stepper_up_timestamp = map(analogRead(ANALOG_Y), 0, 516, 0, 8);
    } 
     else
    {
      stepper_running_up = false;
    }
  }
}

void analog_button()
{
  if (digitalRead(ANALOG_BUTTON) == LOW && interruptTimer > TIMER_TIMESTAMP)
  {
    interruptTimer = 0;
    automatic_mode = !automatic_mode;
    
    if(automatic_mode == true)
    {
      need_calibration = true;
      music_running = true;
    }
    else
    {
      music_running = false;
      colorWipe(strip.Color(255,   0,   0)     , 1); 
    }
    
    Serial.print("Change automatic_mode: "); Serial.println(automatic_mode);
  }
  else
  {
  }
}

void stepper_down_controller(int timestamp)
{
  if(stepperTimer > timestamp)
  {
    stepperTimer = 0;
    if(stepper_running)
    {
      switch(_step)
      { 
       case 0: 
         digitalWrite(Pin1, LOW);  
         digitalWrite(Pin2, LOW); 
         digitalWrite(Pin3, LOW); 
         digitalWrite(Pin4, HIGH); 
       break;  
       case 1: 
         digitalWrite(Pin1, LOW);  
         digitalWrite(Pin2, LOW); 
         digitalWrite(Pin3, HIGH); 
         digitalWrite(Pin4, HIGH); 
       break;  
       case 2: 
         digitalWrite(Pin1, LOW);  
         digitalWrite(Pin2, LOW); 
         digitalWrite(Pin3, HIGH); 
         digitalWrite(Pin4, LOW); 
       break;  
       case 3: 
         digitalWrite(Pin1, LOW);  
         digitalWrite(Pin2, HIGH); 
         digitalWrite(Pin3, HIGH); 
         digitalWrite(Pin4, LOW); 
       break;  
       case 4: 
         digitalWrite(Pin1, LOW);  
         digitalWrite(Pin2, HIGH); 
         digitalWrite(Pin3, LOW); 
         digitalWrite(Pin4, LOW); 
       break;  
       case 5: 
         digitalWrite(Pin1, HIGH);  
         digitalWrite(Pin2, HIGH); 
         digitalWrite(Pin3, LOW); 
         digitalWrite(Pin4, LOW); 
       break;  
         case 6: 
         digitalWrite(Pin1, HIGH);  
         digitalWrite(Pin2, LOW); 
         digitalWrite(Pin3, LOW); 
         digitalWrite(Pin4, LOW); 
       break;  
       case 7: 
         digitalWrite(Pin1, HIGH);  
         digitalWrite(Pin2, LOW); 
         digitalWrite(Pin3, LOW); 
         digitalWrite(Pin4, HIGH); 
       break;  
       default: 
         digitalWrite(Pin1, LOW);  
         digitalWrite(Pin2, LOW); 
         digitalWrite(Pin3, LOW); 
         digitalWrite(Pin4, LOW); 
       break;  
     } 
     if(dir){ 
       _step++; 
     }else{ 
       _step--; 
     } 
     if(_step>7){ 
       _step=0; 
     } 
     if(_step<0){ 
       _step=7; 
     }
    }
  }
}

void stepper_down_controller(bool dir)
{
    if(stepper_running)
    {
      switch(_step)
      { 
       case 0: 
         digitalWrite(Pin1, LOW);  
         digitalWrite(Pin2, LOW); 
         digitalWrite(Pin3, LOW); 
         digitalWrite(Pin4, HIGH); 
       break;  
       case 1: 
         digitalWrite(Pin1, LOW);  
         digitalWrite(Pin2, LOW); 
         digitalWrite(Pin3, HIGH); 
         digitalWrite(Pin4, HIGH); 
       break;  
       case 2: 
         digitalWrite(Pin1, LOW);  
         digitalWrite(Pin2, LOW); 
         digitalWrite(Pin3, HIGH); 
         digitalWrite(Pin4, LOW); 
       break;  
       case 3: 
         digitalWrite(Pin1, LOW);  
         digitalWrite(Pin2, HIGH); 
         digitalWrite(Pin3, HIGH); 
         digitalWrite(Pin4, LOW); 
       break;  
       case 4: 
         digitalWrite(Pin1, LOW);  
         digitalWrite(Pin2, HIGH); 
         digitalWrite(Pin3, LOW); 
         digitalWrite(Pin4, LOW); 
       break;  
       case 5: 
         digitalWrite(Pin1, HIGH);  
         digitalWrite(Pin2, HIGH); 
         digitalWrite(Pin3, LOW); 
         digitalWrite(Pin4, LOW); 
       break;  
         case 6: 
         digitalWrite(Pin1, HIGH);  
         digitalWrite(Pin2, LOW); 
         digitalWrite(Pin3, LOW); 
         digitalWrite(Pin4, LOW); 
       break;  
       case 7: 
         digitalWrite(Pin1, HIGH);  
         digitalWrite(Pin2, LOW); 
         digitalWrite(Pin3, LOW); 
         digitalWrite(Pin4, HIGH); 
       break;  
       default: 
         digitalWrite(Pin1, LOW);  
         digitalWrite(Pin2, LOW); 
         digitalWrite(Pin3, LOW); 
         digitalWrite(Pin4, LOW); 
       break;  
     } 
     if(dir){ 
       _step++; 
     }else{ 
       _step--; 
     } 
     if(_step>7){ 
       _step=0; 
     } 
     if(_step<0){ 
       _step=7; 
     }
    }
}

void stepper_up_controller(int timestamp)
{
  if(stepperUpTimer > timestamp)
  {
    stepperUpTimer = 0;
    if(stepper_running_up)
    {
      switch(_step_up)
      { 
       case 0: 
         digitalWrite(Pin1_HIGH, LOW);  
         digitalWrite(Pin2_HIGH, LOW); 
         digitalWrite(Pin3_HIGH, LOW); 
         digitalWrite(Pin4_HIGH, HIGH); 
       break;  
       case 1: 
         digitalWrite(Pin1_HIGH, LOW);  
         digitalWrite(Pin2_HIGH, LOW); 
         digitalWrite(Pin3_HIGH, HIGH); 
         digitalWrite(Pin4_HIGH, HIGH); 
       break;  
       case 2: 
         digitalWrite(Pin1_HIGH, LOW);  
         digitalWrite(Pin2_HIGH, LOW); 
         digitalWrite(Pin3_HIGH, HIGH); 
         digitalWrite(Pin4_HIGH, LOW); 
       break;  
       case 3: 
         digitalWrite(Pin1_HIGH, LOW);  
         digitalWrite(Pin2_HIGH, HIGH); 
         digitalWrite(Pin3_HIGH, HIGH); 
         digitalWrite(Pin4_HIGH, LOW); 
       break;  
       case 4: 
         digitalWrite(Pin1_HIGH, LOW);  
         digitalWrite(Pin2_HIGH, HIGH); 
         digitalWrite(Pin3_HIGH, LOW); 
         digitalWrite(Pin4_HIGH, LOW); 
       break;  
       case 5: 
         digitalWrite(Pin1_HIGH, HIGH);  
         digitalWrite(Pin2_HIGH, HIGH); 
         digitalWrite(Pin3_HIGH, LOW); 
         digitalWrite(Pin4_HIGH, LOW); 
       break;  
         case 6: 
         digitalWrite(Pin1_HIGH, HIGH);  
         digitalWrite(Pin2_HIGH, LOW); 
         digitalWrite(Pin3_HIGH, LOW); 
         digitalWrite(Pin4_HIGH, LOW); 
       break;  
       case 7: 
         digitalWrite(Pin1_HIGH, HIGH);  
         digitalWrite(Pin2_HIGH, LOW); 
         digitalWrite(Pin3_HIGH, LOW); 
         digitalWrite(Pin4_HIGH, HIGH); 
       break;  
       default: 
         digitalWrite(Pin1_HIGH, LOW);  
         digitalWrite(Pin2_HIGH, LOW); 
         digitalWrite(Pin3_HIGH, LOW); 
         digitalWrite(Pin4_HIGH, LOW); 
       break;  
     } 
     if(dir_up){ 
       _step_up++; 
     }else{ 
       _step_up--; 
     } 
     if(_step_up>7){ 
       _step_up=0; 
     } 
     if(_step_up<0){ 
       _step_up=7; 
     }
    }
  }
}

void stepper_up_controller(bool dir_up)
{
    if(stepper_running_up)
    {
      switch(_step_up)
      { 
       case 0: 
         digitalWrite(Pin1_HIGH, LOW);  
         digitalWrite(Pin2_HIGH, LOW); 
         digitalWrite(Pin3_HIGH, LOW); 
         digitalWrite(Pin4_HIGH, HIGH); 
       break;  
       case 1: 
         digitalWrite(Pin1_HIGH, LOW);  
         digitalWrite(Pin2_HIGH, LOW); 
         digitalWrite(Pin3_HIGH, HIGH); 
         digitalWrite(Pin4_HIGH, HIGH); 
       break;  
       case 2: 
         digitalWrite(Pin1_HIGH, LOW);  
         digitalWrite(Pin2_HIGH, LOW); 
         digitalWrite(Pin3_HIGH, HIGH); 
         digitalWrite(Pin4_HIGH, LOW); 
       break;  
       case 3: 
         digitalWrite(Pin1_HIGH, LOW);  
         digitalWrite(Pin2_HIGH, HIGH); 
         digitalWrite(Pin3_HIGH, HIGH); 
         digitalWrite(Pin4_HIGH, LOW); 
       break;  
       case 4: 
         digitalWrite(Pin1_HIGH, LOW);  
         digitalWrite(Pin2_HIGH, HIGH); 
         digitalWrite(Pin3_HIGH, LOW); 
         digitalWrite(Pin4_HIGH, LOW); 
       break;  
       case 5: 
         digitalWrite(Pin1_HIGH, HIGH);  
         digitalWrite(Pin2_HIGH, HIGH); 
         digitalWrite(Pin3_HIGH, LOW); 
         digitalWrite(Pin4_HIGH, LOW); 
       break;  
         case 6: 
         digitalWrite(Pin1_HIGH, HIGH);  
         digitalWrite(Pin2_HIGH, LOW); 
         digitalWrite(Pin3_HIGH, LOW); 
         digitalWrite(Pin4_HIGH, LOW); 
       break;  
       case 7: 
         digitalWrite(Pin1_HIGH, HIGH);  
         digitalWrite(Pin2_HIGH, LOW); 
         digitalWrite(Pin3_HIGH, LOW); 
         digitalWrite(Pin4_HIGH, HIGH); 
       break;  
       default: 
         digitalWrite(Pin1_HIGH, LOW);  
         digitalWrite(Pin2_HIGH, LOW); 
         digitalWrite(Pin3_HIGH, LOW); 
         digitalWrite(Pin4_HIGH, LOW); 
       break;  
     } 
     if(dir_up){ 
       _step_up++; 
     }else{ 
       _step_up--; 
     } 
     if(_step_up>7){ 
       _step_up=0; 
     } 
     if(_step_up<0){ 
       _step_up=7; 
     }
    }
}

void callibrateSteppers(bool *need_calibration)
{
  if(*need_calibration == true)
  {
    stepper_running= true;
    stepper_running_up = true;
    for(int i = 0; i<2700; i++)
    {
      stepper_down_controller(false);
      delay(1);
    }
    for(int i = 0; i<1300; i++)
    {
      stepper_up_controller(true);
      delay(1);
    }

    for(int i = 0; i<550; i++)
    {
      stepper_up_controller(false);
      delay(1);
    }
    stepper_running = false;
    stepper_running_up = false;
    *need_calibration = false;
  }
}

void moveAward(bool *automatic_mode)
{
  if(*automatic_mode == true)
  {
      stepper_running= true;
      stepper_running_up = true;
      for(int i = 0; i<2700; i++)
      {
        if(*automatic_mode == true)
        {
          colorWipe(strip.Color(255,   0,   255)     , 1); 
          stepper_down_controller(true);
          if(i%5==0)
          {
            stepper_up_controller(true);
          }
          delay(1);
        }
        else
        {
          stepper_running = false;
          stepper_running_up = false;
          colorWipe(strip.Color(255,   0,   0)     , 1); 
          break;
        }
      }

      //MUSIC
      //playMelody(championVocalMelody, championVocalNoteDurations, 67,&music_running);
      playMelody(championVocalMelody, championVocalNoteDurations, 17,&music_running);

      for(int i = 0; i<2700; i++)
      {
        colorWipe(strip.Color(255,   0,   255)     , 1); 
        if(*automatic_mode == true)
        {
          stepper_down_controller(false);
          if(i%5==0)
          {
            stepper_up_controller(false);
          }
          delay(1);
        }
        else
        {
          stepper_running = false;
          stepper_running_up = false;
          colorWipe(strip.Color(255,   0,   0)     , 1); 
          break;
        }
      } 
  }
  else
  {
  colorWipe(strip.Color(255,   0,   0)     , 1); 
  }
}


void playMelody(int *melody, int *noteDurations, int notesLength, bool *music_running)
{
 
  if(*music_running==true)
  {
    for (int thisNote = 0; thisNote < notesLength; thisNote++) 
    {
      if(*music_running==true)
      {
        int noteDuration = 1000 / noteDurations[thisNote];
        tone(BUZZER_PIN, melody[thisNote], noteDuration);
        colorWipe(strip.Color(255,   255,   0)     , 1); 
        
        int pauseBetweenNotes = noteDuration * 1.0;
        delay(pauseBetweenNotes);
        noTone(BUZZER_PIN);
      }
      else
      {
        Serial.println("STOP!");
        break;
      }
    }
  }
}
