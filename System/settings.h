#pragma once
#include <arduino.h>

//--------------------- PINS
#define ANALOG_X A1
#define ANALOG_Y A0
#define ANALOG_BUTTON 2 
#define LED_PIN    3
#define BUZZER_PIN 9

//--------------------- TIMERS
#define TIMER_TIMESTAMP 900 //interrupt
elapsedMillis analogReadTimer;
elapsedMillis interruptTimer;
elapsedMillis stepperTimer;
elapsedMillis stepperUpTimer;
elapsedMillis musicTimer;
elapsedMillis musicDelayTimer;

//--------------------- ANALOG START VALUE
int X_ANALOG_BASE = 0;
int Y_ANALOG_BASE = 0;

//--------------------- LEDS
#define LED_COUNT 1

//--------------------- SYSTEM
volatile bool automatic_mode = false;
volatile bool need_calibration = false;

//--------------------- STEPPER DOWN
int Pin1 = 10; 
int Pin2 = 11; 
int Pin3 = 12; 
int Pin4 = 13;
int _step = 0; 
volatile bool dir = true;// false=clockwise, true=counter clockwise
volatile bool stepper_running = false;
volatile int stepper_timestamp = 0;

//--------------------- STEPPER UP
int Pin1_HIGH = 4; 
int Pin2_HIGH  = 5; 
int Pin3_HIGH  = 6; 
int Pin4_HIGH  = 7;
int _step_up = 0; 
volatile bool stepper_running_up = false;
volatile bool dir_up = true;
volatile int stepper_up_timestamp = 0;

//--------------------- MUSIC
volatile bool music_running = false;
int music_timestamp = 400;
